from flask import Flask, request, send_file
import requests
import numpy as np
import cv2
app = Flask(__name__)


@app.route('/imageprocessing/imageProcessing', methods=['GET'])
def grayScale():
    URL = request.args.get('url')
    r = requests.get(URL)
    with open('./uploads/sample.jpg', 'wb') as f:
        f.write(r.content)
    img = cv2.imread('uploads/sample.jpg', 0)
    cv2.imwrite('./uploads/output.jpg', img)
    denoise()
    return send_file('../uploads/output.jpg', mimetype='image/jpg')
    """cv2.imshow();
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    """
def denoise():
    img = cv2.imread('./uploads/output.jpg')
    dst = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 21)

    cv2.imwrite('./uploads/output.jpg', dst)
    print("before skewed fix")
    skewedFix()
    print("after skewed fix")
    """cv2.imshow('image', dst)
    
    cv2.waitKey(0)
    cv2.destroyAllWindows()"""
    #with open('uploads/output1.jpg', 'wb') as fs:
        #fs.write(img)

def skewedFix():
    image = cv2.imread('./uploads/output.jpg')
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.bitwise_not(gray)
    thresh = cv2.threshold(gray, 0, 255,
                           cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    coords = np.column_stack(np.where(thresh > 0))
    angle = cv2.minAreaRect(coords)[-1]
    if angle < -45:
        angle = -(90 + angle)
    else:
        angle = -angle
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h),
                             flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    cv2.imwrite('./uploads/output.jpg', rotated)
    print("[INFO] angle: {:.3f}".format(angle))
    upscaling()

def upscaling():
    img = cv2.imread('./uploads/output.jpg')
    newx, newy = (img.shape[1] * 4)/3, (img.shape[0] * 4)/3 # new size (w,h)
    newimage = cv2.resize(img, (newx, newy))

    cv2.imwrite('./uploads/output.jpg',newimage)
    print (newimage.shape)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')







